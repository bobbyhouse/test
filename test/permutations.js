var faker = require('faker');

const URL = 'http://localhost:3000?frdebug=true';
// const URL = 'https://funapple2.localtunnel.me/?frdebug=true'

const setup = (client, buttonCSS, form) => (
  client
    .url(URL)
    .waitForElementVisible('body', 1000)
    .click(buttonCSS)
    .waitForElementPresent('iframe', 1000)
    .pause(1000)
    .frame(`fr_formid_${form}`)
    .pause(1000)
);

const clickPreset = (client, pos) => {
  const selector = `#amount-${pos}`;
  return client.waitForElementPresent(selector, 1000)
    .click(selector);
};

const donorInfo = (client) => (
  client.setValue('#firstName', faker.name.firstName())
    .setValue('#lastName', faker.name.lastName())
    .setValue('#email', faker.internet.email())
    .element('css selector', '#phone', (result) => {
        if (result !== -1) {
          client.setValue('#phone', faker.phone.phoneNumber())
        }
    })
    .element('css selector', '#address', (result) => {
        if (result !== -1) {
          client.setValue('#address', faker.address.streetAddress())
        }
    })
    .element('css selector', '#city', (result) => {
        if (result !== -1) {
          client.setValue('#city', faker.address.city())
        }
    })
    .element('css selector', '#state', (result) => {
        if (result !== -1) {
          client.setValue('#state', faker.address.state())
        }
    })
    .element('css selector', '#country', (result) => {
        if (result !== -1) {
          client.setValue('#country', faker.address.country())
        }
    })
    .setValue('#postalCode', faker.address.zipCode())
);

const customizations = (client) => (client);

module.exports = {
  'anonymous, operations tip, form allocation' : function (client) {
      setup(client, 'button.anonymous.operationsTip.formAllocationId', '305')
        .assert.visible('#DonationForm');

      clickPreset(client, 0)
        .click('#tst_submit').assert.visible('#CustomizationsForm');

      customizations(client).click('#tst_submit');

      client.assert.visible('#DonorInfoForm');

      donorInfo(client).click('#tst_submit');
  }
};
