const { Client } = require('pg');

const assert = require('assert')

const program = require('commander');

program
  .version('0.1.0')
  .option('-a, --amount [value]', 'Amount')
  .option('-p --payment_method [value]', 'Payment Method')
  .option('-r --recurring [value]', 'Recurring')
  .option('-m --is_donate_double [value]', 'Is Donate Double')
  .parse(process.argv);



const client = new Client({
  user: 'postgres',
  host: 'localhost',
  database: 'funraise',
  password: 'linklove',
  port: 5432
});

var myArgs = process.argv.slice(2);


client.connect()

const query = 'select d.id, d.amount, d.payment_method, d.recurring, d.billing_first_name, d.billing_last_name, d.billing_zip, d.is_donate_double, d.donate_double_company_name, d.donate_double_employee_email, d.dedication_message, d.dedication_name, d.dedication_email, d.dedication_type, d.is_dedication, d.anonymous, d.operations_tip, d.source_amount, dn.email, dn.postal_code, dn.phone, dn.address, dn.city, dn.state, dn.country from donation d, donor dn  where d.id = (select max(id) from donation) and dn.id = d.donor_id';

client.query(query, (err, res) => {
    const [response] = res.rows;
    if (program.amount) assert.equal(program.amount, response.amount);
    if (program.payment_method) assert.equal(program.payment_method, response.payment_method);
    if (program.recurring) assert.equal(program.recurring, response.recurring);
    if (program.is_donate_double) assert.equal(program.is_donate_double, response.is_donate_double);
    console.log(response);
    client.end()
})
